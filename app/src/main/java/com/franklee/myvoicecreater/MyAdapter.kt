package com.franklee.myvoicecreater

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewDebug
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView

class MyAdapter(val data: ArrayList<SheetData>, context: Context) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    lateinit var holder: MyAdapter.ViewHolder

    init {

    }

    // 取得最後一行的五線譜
    fun lastLine(): ConstraintLayout {
        return data.last().back
    }

    fun lastAllNotes(): ArrayList<ImageView> {
        return data.last().allNotes
    }

    fun lastC(): ImageView {
        return data.last().noteC
    }

    // 取得音符在五線譜的高度
    fun getNoteYPosition(p: Pitch): Float {
        val nowLine = data.last()
        var result = 0f

        when (p) {
            Pitch.lG -> result = nowLine.noteLG.y
            Pitch.lB -> result = nowLine.noteLB.y
            Pitch.lA -> result = nowLine.noteLA.y

            Pitch.C -> result = nowLine.noteC.y
            Pitch.D -> result = nowLine.noteD.y
            Pitch.E -> result = nowLine.noteE.y
            Pitch.F -> result = nowLine.noteF.y
            Pitch.G -> result = nowLine.noteG.y
            Pitch.A -> result = nowLine.noteA.y
            Pitch.B -> result = nowLine.noteB.y

            Pitch.hC -> result = nowLine.noteHC.y
            Pitch.hD -> result = nowLine.noteHD.y
            Pitch.hE -> result = nowLine.noteHE.y
            Pitch.hF -> result = nowLine.noteHF.y
            Pitch.hG -> result = nowLine.noteHG.y
            Pitch.hA -> result = nowLine.noteHA.y
            Pitch.hB -> result = nowLine.noteHB.y
            Pitch.hhC -> result = nowLine.noteHHC.y
            else -> result = nowLine.noteLG.y
        }
        return result
    }

    override fun onCreateViewHolder(view: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(view.context)

        val view = layoutInflater
                .inflate(R.layout.music_sheet, view, false)
        holder = ViewHolder(view)
        holder.setIsRecyclable(false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) {
        val selectSheet = data[index]
        selectSheet.allNotes = holder.allNotes
        selectSheet.back = holder.background
        holder.txvNum.text = (index + 1).toString()

        // 低音域binding
        selectSheet.noteLG = holder.noteLG
        selectSheet.noteLB = holder.noteB
        selectSheet.noteLA = holder.noteLA

        // 中音域binding
        selectSheet.noteC = holder.noteC
        selectSheet.noteD = holder.noteD
        selectSheet.noteE = holder.noteE
        selectSheet.noteF = holder.noteF
        selectSheet.noteG = holder.noteG
        selectSheet.noteA = holder.noteA
        selectSheet.noteB = holder.noteB

        // 高音域binding
        selectSheet.noteHC = holder.noteHC
        selectSheet.noteHD = holder.noteHD
        selectSheet.noteHE = holder.noteHE
        selectSheet.noteHF = holder.noteHF
        selectSheet.noteHG = holder.noteHG
        selectSheet.noteHA = holder.noteHA
        selectSheet.noteHB = holder.noteHB
        selectSheet.noteHHC = holder.noteHHC
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var allNotes = ArrayList<ImageView>()
        var background: ConstraintLayout
        var txvNum: TextView
        // 低音域
//        var noteLC: ImageView
//        var noteLD: ImageView
//        var noteLE: ImageView
//        var noteLF: ImageView
        var noteLG: ImageView
        var noteLA: ImageView
        var noteLB: ImageView

        // 中音域
        var noteC: ImageView
        var noteD: ImageView
        var noteE: ImageView
        var noteF: ImageView
        var noteG: ImageView
        var noteA: ImageView
        var noteB: ImageView

        // 高音域
        var noteHC: ImageView
        var noteHD: ImageView
        var noteHE: ImageView
        var noteHF: ImageView
        var noteHG: ImageView
        var noteHA: ImageView
        var noteHB: ImageView
        var noteHHC: ImageView

        init {
            background = view.findViewById(R.id.sheetBack)

            noteLG = view.findViewById(R.id.noteLG)
            noteLB = view.findViewById(R.id.noteLB)
            noteLA = view.findViewById(R.id.noteLA)

            noteC = view.findViewById(R.id.noteC)
            noteD = view.findViewById(R.id.noteD)
            noteE = view.findViewById(R.id.noteE)
            noteF = view.findViewById(R.id.noteF)
            noteG = view.findViewById(R.id.noteG)
            noteA = view.findViewById(R.id.noteA)
            noteB = view.findViewById(R.id.noteB)

            noteHC = view.findViewById(R.id.noteHC)
            noteHB = view.findViewById(R.id.noteHB)
            noteHD = view.findViewById(R.id.noteHD)
            noteHE = view.findViewById(R.id.noteHE)
            noteHF = view.findViewById(R.id.noteHF)
            noteHG = view.findViewById(R.id.noteHG)
            noteHA = view.findViewById(R.id.noteHA)
            noteHB = view.findViewById(R.id.noteHB)
            noteHHC = view.findViewById(R.id.noteHHC)

            txvNum = view.findViewById(R.id.txvNum)
        }
    }
}

class MyManager(context: Context) : LinearLayoutManager(context) {
    init {

    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State?) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (e: Error) {
            e.printStackTrace();
        }
    }

}

class MyLowAdapter(val data: ArrayList<SheetData>, context: Context) : RecyclerView.Adapter<MyLowAdapter.LowViewHolder>() {

    lateinit var holder: MyLowAdapter.LowViewHolder

    init {

    }

    // 取得最後一行的五線譜
    fun lastLine(): ConstraintLayout {
        return data.last().back
    }

    fun lastAllNotes(): ArrayList<ImageView> {
        return data.last().allNotes
    }


    // 取得音符在五線譜的高度
    fun getNoteYPosition(p: Pitch): Float {
        val nowLine = data.last()
        var result = 0f

        when (p) {
            Pitch.llE -> result = nowLine.noteLLE.y
            Pitch.llF -> result = nowLine.noteLLF.y
            Pitch.llG -> result = nowLine.noteLLG.y
            Pitch.llA -> result = nowLine.noteLLA.y
            Pitch.llB -> result = nowLine.noteLLB.y

            Pitch.lC -> result = nowLine.noteLC.y
            Pitch.lD -> result = nowLine.noteLD.y
            Pitch.lE -> result = nowLine.noteLE.y
            Pitch.lF -> result = nowLine.noteLF.y
            Pitch.lG -> result = nowLine.noteLG.y
            Pitch.lB -> result = nowLine.noteLB.y
            Pitch.lA -> result = nowLine.noteLA.y

            Pitch.C -> result = nowLine.noteC.y
            Pitch.D -> result = nowLine.noteD.y

            else -> result = nowLine.noteLG.y
        }
        return result
    }

    override fun onCreateViewHolder(view: ViewGroup, p1: Int): LowViewHolder {
        val layoutInflater = LayoutInflater.from(view.context)

        val view = layoutInflater
                .inflate(R.layout.music_sheet_down, view, false)
        holder = LowViewHolder(view)
        holder.setIsRecyclable(false)

        return LowViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: LowViewHolder, index: Int) {
        val selectSheet = data[index]
        selectSheet.allNotes = holder.allNotes
        selectSheet.back = holder.background
        holder.txvNum.text = (index + 1).toString()

        // 低低音域
        selectSheet.noteLLE = holder.noteLLE
        selectSheet.noteLLF = holder.noteLLF
        selectSheet.noteLLG = holder.noteLLG
        selectSheet.noteLLA = holder.noteLLA
        selectSheet.noteLLB = holder.noteLLB

        // 低音域binding
        selectSheet.noteLC = holder.noteLC
        selectSheet.noteLD = holder.noteLD
        selectSheet.noteLE = holder.noteLE
        selectSheet.noteLF = holder.noteLF
        selectSheet.noteLG = holder.noteLG
        selectSheet.noteLB = holder.noteLB
        selectSheet.noteLA = holder.noteLA

        // 中音域binding
        selectSheet.noteC = holder.noteC
        selectSheet.noteD = holder.noteD

    }


    inner class LowViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var allNotes = ArrayList<ImageView>()
        var background: ConstraintLayout
        var txvNum: TextView

        // 低低音域
        var noteLLE: ImageView
        var noteLLF: ImageView
        var noteLLG: ImageView
        var noteLLA: ImageView
        var noteLLB: ImageView

        // 低音域
        var noteLC: ImageView
        var noteLD: ImageView
        var noteLE: ImageView
        var noteLF: ImageView
        var noteLG: ImageView
        var noteLA: ImageView
        var noteLB: ImageView

        // 中音域
        var noteC: ImageView
        var noteD: ImageView

        init {
            background = view.findViewById(R.id.sheetBack)

            noteLLE = view.findViewById(R.id.noteLLE)
            noteLLF = view.findViewById(R.id.noteLLF)
            noteLLG = view.findViewById(R.id.noteLLG)
            noteLLA = view.findViewById(R.id.noteLLA)
            noteLLB = view.findViewById(R.id.noteLLB)

            noteLC = view.findViewById(R.id.noteLC)
            noteLD = view.findViewById(R.id.noteLD)
            noteLE = view.findViewById(R.id.noteLE)
            noteLF = view.findViewById(R.id.noteLF)
            noteLG = view.findViewById(R.id.noteLG)
            noteLB = view.findViewById(R.id.noteLB)
            noteLA = view.findViewById(R.id.noteLA)

            noteC = view.findViewById(R.id.noteC)
            noteD = view.findViewById(R.id.noteD)

            txvNum = view.findViewById(R.id.txvNum)
        }
    }
}
