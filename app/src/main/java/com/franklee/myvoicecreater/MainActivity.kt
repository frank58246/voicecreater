package com.franklee.myvoicecreater

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Display
import android.view.View
import android.widget.ImageView
import android.widget.ListView
import android.widget.Toast
import be.tarsos.dsp.io.android.AudioDispatcherFactory
import be.tarsos.dsp.pitch.PitchProcessor.PitchEstimationAlgorithm
import be.tarsos.dsp.pitch.PitchProcessor
import be.tarsos.dsp.pitch.PitchDetectionHandler
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.music_sheet.view.*


class MainActivity : AppCompatActivity() {
    var audioThread = Thread()
    var mDuringTime = 0
    var spectrum: StringBuilder = StringBuilder()
    var pitches = Pitch.values().reversed()
    var nowPitch = Pitch.hhC
    var lastPitchX = 0f
    var mTotalNote = 0f
    var mNowRealHz = 0f
    var lastRealHz = 0f


    lateinit var recyclerView: RecyclerView
    lateinit var myAdapter: MyLowAdapter
    var dataCore = ArrayList<SheetData>()
    var testX = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 確認權限是否有開啟
        val permission: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    permission, 1)
        } else {
            startDetecting()
        }

        dataCore.add(SheetData(this))

        // 初始化樂譜的recyclerView
        recyclerView = this.mainView
        recyclerView.layoutManager =MyManager(this)
        myAdapter = MyLowAdapter(dataCore, this)
        recyclerView.adapter = myAdapter
        // 要擴大recycler view的快取，否則畫面會錯亂
        recyclerView.setItemViewCacheSize(16)



        button.setOnClickListener {
            mDuringTime = 40
            addNote(Pitch.hC)

        }

    }


    // 開始偵測
    fun startDetecting() {
        val dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(22050, 1024, 0)

        val pdh = PitchDetectionHandler { res, e ->
            val pitchInHz = res.pitch
            runOnUiThread { processPitch(pitchInHz) }
        }
        val pitchProcessor = PitchProcessor(PitchEstimationAlgorithm.FFT_YIN, 22050f, 1024, pdh)
        dispatcher.addAudioProcessor(pitchProcessor)

        audioThread = Thread(dispatcher, "Audio Thread")
        audioThread.start()
    }


    // 偵測音高
    fun processPitch(pitchInHz: Float) {
        var sensePitch : Pitch? = null
        var senseHz = pitchInHz
        // 如果跟上一個音高差不多，視為前同一個音高
        if(Math.abs(lastRealHz -pitchInHz ) < 10 ){
            senseHz = lastRealHz
        }
        // 把偵測到的音高存下來
        lastRealHz = senseHz
        for (i in pitches) {
            if (senseHz > i.value) {
                nowNote.text = i.toString()
                sensePitch = i
                break
            }
        }
        // 有對應的音高才進入方法
        if (sensePitch != null ) {
            calculatTime(sensePitch)
            pitchText.text = String.format("%f for %d ",pitchInHz,mDuringTime)
        }

    }

    fun calculatTime(p: Pitch) {
        mDuringTime++

        var aa = myAdapter.data.last()
        Log.d("test", String.format("%s at %f",aa.noteLLE.id,aa.noteLLE.y))
        Log.d("test", String.format("%s at %f",aa.noteLLF.id,aa.noteLLF.y))
        Log.d("test", String.format("%s at %f",aa.noteLLG.id,aa.noteLLG.y))
        Log.d("test", String.format("%s at %f",aa.noteLLA.id,aa.noteLLA.y))
        Log.d("test", String.format("%s at %f",aa.noteLLB.id,aa.noteLLB.y))
        Log.d("test", String.format("%s at %f",aa.noteLC.id,aa.noteLC.y))
        Log.d("test", String.format("%s at %f",aa.noteLD.id,aa.noteLD.y))
        Log.d("test", String.format("%s at %f",aa.noteLE.id,aa.noteLE.y))
        Log.d("test", String.format("%s at %f",aa.noteLF.id,aa.noteLF.y))
        Log.d("test", String.format("%s at %f",aa.noteLG.id,aa.noteLG.y))
        Log.d("test", String.format("%s at %f",aa.noteLA.id,aa.noteLA.y))
        Log.d("test", String.format("%s at %f",aa.noteLB.id,aa.noteLB.y))
        Log.d("test", String.format("%s at %f",aa.noteC.id,aa.noteC.y))



        if (nowPitch == p ) {
            changeNotePicture(p,mDuringTime)
            if(mDuringTime > 64){
                addNote(p)
                nowPitch = p
                mDuringTime = 1
            }
        }else{
            addNote(p)
            nowPitch = p
            mDuringTime = 1
        }

        nowNote.text = "${p}_$mDuringTime"
    }

    /**
     * 把音符畫到五線譜上面
     */
    fun addNote(pitch: Pitch) = runOnUiThread {
        if (mDuringTime < 4) {
            return@runOnUiThread
        }

        // 取得音符的圖形
        var note = ImageView(this)
        var drawable = getNotePic(pitch, mDuringTime)
        note.setImageDrawable(drawable)

        // 取得目前最新一行的五線譜

        val lineWidth = (myAdapter.data.first().back.width).toFloat()
        val noteWidth = myAdapter.data.last().noteA.width.toFloat()

        // 指定音符要出現的X軸位置
        if (lastPitchX == 0f) {
            lastPitchX = lineWidth / 10
        }
        lastPitchX += lineWidth / 10
        note.measure(0, 0)

        // 如果這一行畫滿了，新增一條五線譜
        if (lastPitchX > (lineWidth - note.measuredWidth / 2f)) {

            for(i in myAdapter.lastAllNotes()){
                Log.d("test",String.format(" at %f",i.y))
            }


            ((recyclerView.adapter!!) as MyLowAdapter).data.add(SheetData(this))
            recyclerView.adapter!!.notifyItemChanged(recyclerView.adapter!!.itemCount-1)
            lastPitchX = lineWidth / 10
            recyclerView.scrollToPosition(recyclerView.adapter!!.itemCount-1)


        }

        val nowLine = myAdapter.lastLine()
        val linePoint = myAdapter.data.last().allPoints
        val lineNotes = myAdapter.lastAllNotes()
        nowLine.addView(note)
        note.measure(0, 0)
        note.x = lastPitchX - note.measuredWidth / 2
        note.y = myAdapter.getNoteYPosition(pitch)

        linePoint.add(NotePoint(note.x, note.y))
        lineNotes.add(note)
    }

    /**
     * 切換畫面上音符的長度
     */
    fun changeNotePicture(pitch: Pitch,time: Int ){
        // 如果沒有最後一個音符就退出，正常來說應該是不會啦
        if( myAdapter.lastAllNotes().count() == 0){
            return
        }
        var lastNote=  myAdapter.lastAllNotes().last()
        lastNote.setImageDrawable(getNotePic(pitch,time))
    }

    /**
     * 取得音符圖片
     */
    fun getNotePic(p: Pitch, time: Int): Drawable {
        // 判斷音符是否需要倒放
        var id = 0
        //return resources.getDrawable(R.drawable.note_up_half, null)
        if (p.value < Pitch.lD.value) {
            // 根據時間長短，判斷為音符長短
            when (time / 8) {
                0 -> {
                    id = R.drawable.note_up_hex
                }
                1 -> {
                    id = R.drawable.note_up_oct
                }
                2 -> {
                    id = R.drawable.note_up_quater
                }
                3 -> {
                    id = R.drawable.note_up_quater
                }
                4, 5 -> {
                    id = R.drawable.note_up_half
                }
                6 -> {
                    id = R.drawable.note_up_half
                }
                else -> {
                    id = R.drawable.note_whole
                }
            }
        } else {
            when (time / 8) {
                0 -> {
                    id = R.drawable.note_down_hex
                }
                1 -> {
                    id = R.drawable.note_down_oct
                }
                2 -> {
                    id = R.drawable.note_down_quater
                }
                3 -> {
                    id = R.drawable.note_down_half
                }
                4, 5 -> {
                    id = R.drawable.note_down_half
                }
                6 -> {
                    id = R.drawable.note_down_half
                }
                else -> {
                    id = R.drawable.note_whole
                }
            }
        }
        return resources.getDrawable(id, null)
    }


    // 確認使用者打開麥克風
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startDetecting()
            } else {
                // Permission Denied
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

enum class Pitch(val value: Double) {
    llC(65.4),
    llD(73.4),
    llE(82.4),
    llF(87.3),
    llG(98.0),
    llA(110.0),
    llB(123.5),
    lC(130.81),
    lD(146.83),
    lE(164.81),
    lF(174.61),
    lG(196.00),
    lA(220.00),
    lB(246.94),
    C(261.63),
    D(293.66),
    E(329.63),
    F(349.23),
    G(392.00),
    A(440.00),
    B(493.88),
    hC(523.25),
    hD(587.33),
    hE(659.26),
    hF(698.46),
    hG(783.99),
    hA(880.00),
    hB(987.77),
    hhC(1046.5)
}

class Note(val pitch: Pitch, val time: Double)