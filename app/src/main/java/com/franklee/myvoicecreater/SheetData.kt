package com.franklee.myvoicecreater

import android.content.Context
import android.media.Image
import android.support.constraint.ConstraintLayout
import android.widget.ImageView


class NotePoint(var x: Float, var y : Float)
class SheetData( context: Context) {
    var allNotes = ArrayList<ImageView>()
    var allPoints = ArrayList<NotePoint>()
    var highlightNum = 0
    // 五線譜
    var back: ConstraintLayout = ConstraintLayout(context)

    // 低低音域
    var noteLLE :ImageView = ImageView(context)
    var noteLLF :ImageView = ImageView(context)
    var noteLLG :ImageView = ImageView(context)
    var noteLLA :ImageView = ImageView(context)
    var noteLLB :ImageView = ImageView(context)

    // 低音域
    var noteLC: ImageView = ImageView(context)
    var noteLD: ImageView = ImageView(context)
    var noteLE: ImageView= ImageView(context)
    var noteLF: ImageView= ImageView(context)
    var noteLG: ImageView= ImageView(context)
    var noteLA: ImageView= ImageView(context)
    var noteLB: ImageView= ImageView(context)

    // 中音域
    var noteC: ImageView= ImageView(context)
    var noteD: ImageView= ImageView(context)
    var noteE: ImageView= ImageView(context)
    var noteF: ImageView= ImageView(context)
    var noteG: ImageView= ImageView(context)
    var noteA: ImageView= ImageView(context)
    var noteB: ImageView= ImageView(context)

    // 高音域
    var noteHC : ImageView= ImageView(context)
    var noteHD : ImageView= ImageView(context)
    var noteHE : ImageView= ImageView(context)
    var noteHF : ImageView= ImageView(context)
    var noteHG : ImageView= ImageView(context)
    var noteHA : ImageView= ImageView(context)
    var noteHB : ImageView= ImageView(context)
    var noteHHC : ImageView = ImageView(context)
    init {

    }
}

//const val llC = 0
//const val llD = 0
//const val llE = 0
//const val llF = 0
//const val llG = 0
//const val llA = 0
//const val llB = 0
//const val lC = 0
//const val lD = 0
//const val lE = 0
//const val lF = 0
//const val lG = 0
//const val lA = 0
//const val lB = 0
//const val C = 0
//const val D = 0
//const val E = 0
//const val F = 0
//const val G = 0
//const val A = 0
//const val B = 0
//const val hC = 0
//const val hD = 0
//const val hE = 0
//const val hF = 0
//const val hG = 0
//const val hA = 0
//const val hB = 0
//const val hhC = 0